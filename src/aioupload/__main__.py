import asyncio
import logging.config

from aioupload import settings
from aioupload.server import Server


def main():
    logging.config.dictConfig(settings.LOGGING)
    loop = asyncio.get_event_loop()
    app = Server(debug=settings.DEBUG, loop=loop)
    app.run()


if __name__ == '__main__':
    main()
