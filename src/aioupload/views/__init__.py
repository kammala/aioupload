from aioupload.views.download import Download
from aioupload.views.ui import Index, List
from aioupload.views.upload import Upload

__all__ = [
    'Download',
    'Upload',
    'List',
    'Index',
]
