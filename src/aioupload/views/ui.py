import logging
import os
from json.encoder import JSONEncoder
from typing import Tuple

from aiohttp.web_reqrep import Response
from aiohttp.web_urldispatcher import View
from aioupload.views.common import MetadataMixin

logger = logging.getLogger(__name__)


class TemplateMixin:
    def get_template_body(self, filename: str) -> str:
        # shitty code
        import aioupload
        root = os.path.dirname(aioupload.__file__)
        templ = os.path.join(root, 'templates')
        with open(os.path.join(templ, filename)) as m:
            template = m.read()
        return template


class Index(TemplateMixin, View):
    async def get(self):
        template = self.get_template_body('main.html')
        return Response(
            text=template, content_type='text/html',
        )


class List(MetadataMixin, TemplateMixin, View):
    async def get(self):
        page_template = self.get_template_body('list.html')
        raw_data = [
            {
                'url': self.request.app.router.named_resources().get('download').url_for(hashedname=hashed_name).path,
                'real_filename': meta.get('real_filename', 'unknown'),
                'uploaded_at': meta.get('ts').isoformat(),
                'uuid': meta.get('uuid').hex,
                'len': '{}&nbsp;{}'.format(*self.humanize_size(meta.get('len'))),
            }
            for hashed_name, meta in sorted(
                self.metadata.items(),
                key=lambda x: (
                    -x[1].get('ts', None).timestamp(),
                    x[1].get('real_filename', None)
                )
            )
        ]

        encoder = JSONEncoder()

        return Response(
            text=page_template.format(serialized_listing=encoder.encode(raw_data)),
            content_type='text/html', charset='utf8'
        )

    def humanize_size(self, size: int) -> Tuple[str, str]:
        if not isinstance(size, int):
            return '&mdash;', ''
        known_suffixes = ('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB')
        bytes_in_suffix = (2 ** (10 * i) for i in range(len(known_suffixes)))
        size_in_suffix = (size // i for i in bytes_in_suffix)
        return [x for x in zip(size_in_suffix, known_suffixes) if x[0] >= 1][-1]
