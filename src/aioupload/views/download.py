import logging
import os
from typing import Iterator

from aiohttp.web_reqrep import Response, StreamResponse
from aiohttp.web_urldispatcher import View
from aioupload import settings
from aioupload.views.common import MetadataMixin

logger = logging.getLogger(__name__)


class IOException(RuntimeError):
    pass


class NotFound(RuntimeError):
    pass


class Download(MetadataMixin, View):
    async def get(self):
        hashed_name = self.request.match_info.get('hashedname', None)
        try:
            logger.info('Trying to obtain real name for %s', hashed_name)
            real_filename = await self._get_real_filename(hashed_name)
        except NotFound:
            logger.warning('Failed to obtain real name for %s', hashed_name, exc_info=True)
            return Response(status=404, text='File with hash "{}" is not registered.'.format(hashed_name))

        logger.info('Getting %s [%s]', real_filename, hashed_name)
        try:
            fullpath = self._get_full_path(hashed_name)
        except IOException:
            logger.warning('Failed to read from file %s', real_filename)
            return Response(status=500, text='File "{}" is unreadable.'.format(real_filename))
        resp = StreamResponse()
        resp.headers['Content-Disposition'] = 'attachment; filename="{}"'.format(real_filename)
        await resp.prepare(self.request)
        for chunk in self._read_file_by_chunks(fullpath):
            resp.write(chunk)
            await resp.drain()
        return resp

    async def _get_real_filename(self, hashed: str) -> str:
        meta = self.metadata.get(hashed, None)
        if meta is None:
            raise NotFound(hashed)
        return meta.get('real_filename')

    def _read_file_by_chunks(self, fullpath: str) -> Iterator[bytes]:
        with open(fullpath, 'rb') as fileobj:
            res = fileobj.read(settings.DOWNLOAD_READ_CHUNK_SIZE)
            while res:
                yield res
                res = fileobj.read(settings.DOWNLOAD_READ_CHUNK_SIZE)

    def _get_full_path(self, filename: str) -> str:
        target_dir = settings.TARGET_DIR
        if not os.path.exists(target_dir):
            raise IOException('No such target directory')
        elif not os.path.isdir(target_dir):
            raise IOException('Invalid target directory')
        fullpath = os.path.join(target_dir, filename)
        if not os.path.exists(fullpath):
            raise IOException('No such file')
        return fullpath
