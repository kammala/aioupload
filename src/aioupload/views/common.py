class MetadataMixin:
    @property
    def metadata(self):
        return self.request.app.metadata
