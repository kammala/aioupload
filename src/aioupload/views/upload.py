import hashlib
import http
import os
import shutil
import tempfile
import uuid
from datetime import datetime

import aiohttp
from aiohttp.helpers import parse_mimetype
from aiohttp.multipart import BodyPartReader
from aiohttp.web_exceptions import HTTPTemporaryRedirect
from aiohttp.web_reqrep import Response
from aiohttp.web_urldispatcher import View
from aioupload import settings
from aioupload.views.common import MetadataMixin


class Upload(MetadataMixin, View):
    async def post(self):
        maintype, *_ = parse_mimetype(self.request.headers[aiohttp.hdrs.CONTENT_TYPE])
        if maintype != 'multipart':
            return Response(status=http.HTTPStatus.BAD_REQUEST, text='Only multipart data is supported')
        rdr = await self.request.multipart()
        async for part in rdr:
            await self._process(part)
        return HTTPTemporaryRedirect('/')

    async def _process(self, part: BodyPartReader):
        with tempfile.TemporaryFile(mode='rb+') as tmp:
            hasher = hashlib.md5()
            datalen = 0
            while not part.at_eof():
                data = await part.read_chunk(size=settings.UPLOAD_READ_CHUNK_SIZE)
                datalen += len(data)
                hasher.update(data)
                tmp.write(data)
            if datalen == 0 and not part.filename:
                # exit if there is no file
                return
            # using uuid instead of ts + hash may be more actual here
            content_hash = hasher.hexdigest()
            await self._write_to_disk(content_hash, datalen, part.filename, tmp)

    async def _write_to_disk(self, content_hash, datalen, src_name, tmp):
        fileuuid = uuid.uuid4()
        ts = datetime.utcnow()
        storage_name = '{ts}-{hash}.bin'.format(hash=content_hash, ts=ts.timestamp())
        path = os.path.join(settings.TARGET_DIR, storage_name)
        with open(path, mode='wb') as dst:
            tmp.seek(0)
            shutil.copyfileobj(tmp, dst)

        # dump info to db
        self.metadata[storage_name] = {
            'real_filename': src_name,
            'hash': content_hash,
            'uuid': fileuuid,
            'ts': ts,
            'len': datalen,
        }

    async def put(self):
        return await self.post()
