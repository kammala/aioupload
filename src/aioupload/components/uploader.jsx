class SingleFileUploader extends React.Component {
    constructor(props) {
        super(props);
        this.remove = this.remove.bind(this);
    }

    remove() {
        this.props.uploader.removeFile(this.props.uid);
    }

    render() {
        const file_num = 'file-' + this.props.uid;
        return (
            <div className={[file_num, 'file', 'row'].join(' ')}>
                <div className='col-xs-6'>
                    <input type='file' name={file_num}/>
                </div>
                <div className='col-xs-4 offset-xs-2'>
                    <button className='btn btn-danger' type='button' onClick={this.remove}>
                        <i className='fa fa-trash-o'/>&nbsp;Remove
                    </button>
                </div>
            </div>
        );
    }
}

class ControlBtns extends React.Component {
    render() {
        return (
            <div className='control-buttons row'>
                <div className='col-xs-6 btn-group'>
                    <button className='btn btn-danger' type='reset'>
                        <i className='fa fa-ban'/>&nbsp;Cancel all
                    </button>
                    <button className='btn btn-primary' type='button' onClick={this.props.uploader.addFile}>
                        <i className='fa fa-plus'/>&nbsp;Add one more
                    </button>
                </div>
                <div className='col-xs-4 offset-xs-2 btn-group'>
                    <button className='btn btn-success' type='submit'>
                        <i className='fa fa-cloud-upload'/>&nbsp;Upload
                    </button>
                </div>
            </div>
        );
    }
}


class Uploader extends React.Component {
    constructor(props) {
        super(props);
        const new_items_cnt = parseInt(props.number);
        this.state = {
            number: new_items_cnt,
            files: this.generateFiles(new_items_cnt),
        };
        this.addFile = this.addFile.bind(this);
        this.removeFile = this.removeFile.bind(this);

    }

    addFile() {
        const cnt = 1;
        this.setState({
            number: this.state.number + cnt,
            files: this.state.files.concat(this.generateFiles(cnt)),
        });
        this.render();
    }

    removeFile(removed_uid) {
        const removed_idx = this.state.files.findIndex(
            (obj) => {
                return obj.props.uid == removed_uid
            }
        );
        this.state.files.splice(removed_idx, 1);
        this.setState({
            files: this.state.files,
            number: this.state.number - 1,
        });
        this.render();
    }

    generateFiles(cnt) {
        return [...Array(cnt).keys()].map(() => {
            const new_id = Math.floor(Math.random() * 100) + 1;
            return (
                <SingleFileUploader
                    uploader={this}
                    uid={new_id}
                    key={new_id}
                />
            );
        })
    }

    render() {
        return (
            <div className='uploader container'>
                <form action='/-/upload/' method='POST' encType='multipart/form-data'>
                    {this.state.files}
                    <ControlBtns uploader={this}/>
                </form>
            </div>
        );
    }
}


ReactDOM.render(
    <Uploader number='3'/>,
    document.getElementById('uploader')
);
