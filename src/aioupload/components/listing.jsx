class Listing extends React.Component {
    render() {
        return (
            <tr className='row'>
                <td>{this.props.data.real_filename}</td>
                <td dangerouslySetInnerHTML={{__html: this.props.data.len}}/>
                <td>
                    <div className='btn-group'>
                        <a href={this.props.data.url} className='btn btn-primary'>
                            <i className='fa fa-cloud-download'/>&nbsp;Download
                        </a>
                        <button className='btn btn-danger' onClick={() => alert('Not implemented')}>
                            <i className='fa fa-trash-o'/>&nbsp;Delete
                        </button>
                    </div>
                </td>
            </tr>
        )
    }
}

class FileList extends React.Component {
    get_data() {
        // we inline data in page instead of ajaxing it
        return LISTING;
    }

    render() {
        const listings = this.get_data().map((data) => {
            return <Listing data={data} key={data.uuid}/>
        });
        return (
            <div className='container'>
                <table className='table table-striped'>
                    {listings}
                </table>
            </div>
        )
    }
}

ReactDOM.render(
    <FileList/>,
    document.getElementById('listing')
);
