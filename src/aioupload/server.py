import asyncio
import logging
import shelve

from aiohttp.web import Application
from aioupload import settings

logger = logging.getLogger(__name__)


class Server(Application):
    def run(self):
        logger.info('Starting server :: %s:%s', settings.HOST, settings.PORT)
        self.register_urls()
        hdlr = self.make_handler()
        server = self.loop.create_server(
            hdlr, settings.HOST, settings.PORT,
        )
        srv, startup_res = self.loop.run_until_complete(
            asyncio.gather(
                server, self.startup(),
                loop=self.loop
            )
        )
        self.open_metadata(settings.METADATA_FILE)

        try:
            self.loop.run_forever()
        except KeyboardInterrupt:  # pragma: no cover
            pass
        finally:
            logger.info('Shutting down the server')
            srv.close()
            self.close_metadata()
            self.loop.run_until_complete(srv.wait_closed())
            self.loop.run_until_complete(self.shutdown())
            self.loop.run_until_complete(hdlr.finish_connections(settings.SHUTDOWN_TIMEOUT))
            self.loop.run_until_complete(self.cleanup())

    def register_urls(self):
        from aioupload.views import Index, List, Upload, Download
        self.router.add_resource('/', name='index').add_route('GET', Index)
        self.router.add_resource('/list/', name='list').add_route('GET', List)
        up_res = self.router.add_resource('/-/upload/', name='upload')
        up_res.add_route('POST', Upload)
        up_res.add_route('PUT', Upload)
        self.router.add_resource('/-/download/{hashedname}', name='download').add_route('GET', Download)
        static_path = self._get_static_path()
        self.router.add_static('/static/', static_path, name='static')

    def _get_static_path(self):
        import aioupload, os
        return os.path.join(os.path.dirname(aioupload.__file__), 'static')

    def open_metadata(self, path):
        self.metadata = shelve.open(path, writeback=True)

    def close_metadata(self):
        self.metadata.close()
