import os

TRUE_STRINGS = {'1', 'on', 'true'}

DEBUG = os.environ.get('DEBUG', 'False').lower() in TRUE_STRINGS

HOST = os.environ.get('HOST', '127.0.0.1')
PORT = os.environ.get('PORT', '7323')
SHUTDOWN_TIMEOUT = float(os.environ.get('SHUTDOWN_TIMEOUT', 10.0))

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s (%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'null': {
            'class': 'logging.NullHandler',
        }
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    },
}

DEFAULT_TARGET_DIR = os.path.expanduser(os.path.join('~', '.aioup'))
TARGET_DIR = os.environ.get('TARGET_DIR', DEFAULT_TARGET_DIR)
DEFAULT_METADATA_FILENAME = '.db'
METADATA_FILENAME = os.environ.get('METADATA_FILENAME', DEFAULT_METADATA_FILENAME)
METADATA_FILE = os.path.join(TARGET_DIR, METADATA_FILENAME)

DEFAULT_CHUNK_SIZE = 200 * 1024  # 200KB in bytes
DOWNLOAD_READ_CHUNK_SIZE = os.environ.get('READ_CHUNK_SIZE', DEFAULT_CHUNK_SIZE)
UPLOAD_READ_CHUNK_SIZE = os.environ.get('READ_CHUNK_SIZE', DEFAULT_CHUNK_SIZE)
UPLOAD_WRITE_CHUNK_SIZE = os.environ.get('READ_CHUNK_SIZE', DEFAULT_CHUNK_SIZE)
