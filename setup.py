from setuptools import find_packages, setup

setup(
    name='aioupload',
    version='0.0.1',
    author='Pavel Kamaev',
    author_email='kamaevpp@gmail.com',
    description='Asynchronous uploading server',
    long_description='',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    include_package_data=True,
    zip_safe=False,
    package_data={},
    classifiers=[
        'Programming Language :: Python :: 3.5',
    ],
    install_requires=[
        'aiohttp >= 1.0, <2.0',
    ],
    entry_points={
        'console_scripts': [
            'aioupload = aioupload.__main__:main',
        ],
    },
)
